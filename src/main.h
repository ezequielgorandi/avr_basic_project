
/**
@defgroup gorandi_main Main
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <util/delay.h>
#include <avr/sleep.h>
#include "time.h"

#include "peripherals/timer/timer0.h"
#include "peripherals/timer/timer1.h"
#include "peripherals/timer/timerStructure.h"
#include "peripherals/serial/uart.h"
#include "peripherals/serial/spi.h"
#include "peripherals/adc.h"
#include "peripherals/IOtools.h"
#include "peripherals/serial/i2c/i2c_master.h"
#include "tools/timeTools.h"
#include "tools/asciiTools.h"
#include "tools/stringTools.h"
#include "hd4478/lcd.h"
#include "constants.h"
#include "serialComunication.h"

