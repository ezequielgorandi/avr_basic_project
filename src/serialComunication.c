/*
 * serialComunication.c
 *
 * Created: 26/09/2017 20:46:32
 *  Author: Ezequiel Gorandi
 *  e-mail: ezequielgorandi@gmail.com
 */ 

#include "serialComunication.h"

unsigned int (*ptr_uart_getc) (void);
unsigned char (*ptr_uart_getChar)(char *);
void (*ptr_uart_puts)(const char *);
void (*ptr_uart_putc)(unsigned char );


void comConfig (
	unsigned int (*aPtr_uart_getc) (void),
	unsigned char (*aPtr_uart_getChar)(char *),
	void (*aPtr_uart_puts)(const char *),
	void (*aPtr_uart_putc)(unsigned char )
		  )
{
	ptr_uart_getc = aPtr_uart_getc;
	ptr_uart_getChar = aPtr_uart_getChar;
	ptr_uart_puts = aPtr_uart_puts ;
	ptr_uart_putc = aPtr_uart_putc ;
}

void confUart_getChar (char * uart_value)
{
	ptr_uart_getChar(uart_value);
}
RESULT sendComand(uint8_t aCommand, uint8_t responseExpected, uint16_t timeout)
{
	uint8_t answer=0;
	UART_TXBYTE(aCommand);    // Send the AT command
	// this loop waits for the answer
	SET_TIMEOUT_MS((uint8_t)SEND_COMMAND_TIMEOUT,timeout);
	do{
		// if there are data in the UART input buffer, reads it and checks for the asnwer
		answer = UART_RXBYTE();
	}
	// Waits for the asnwer with time out
	while((answer != responseExpected) && !TIME_OUT(SEND_COMMAND_TIMEOUT));
	if (answer == responseExpected)
	{
		return RECIEVE_OK;		
	} 
	else
	{
		return RECIEVE_ERROR;
	}
}

RESULT sendComandString(char * aCommand, uint8_t responseExpected, uint16_t timeout)
{
	uint8_t  answer=0;
	ptr_uart_puts(aCommand);    // Send the AT command
	// this loop waits for the answer
	SET_TIMEOUT_MS((uint8_t)SEND_COMMAND_TIMEOUT,timeout);
	do{
		// if there are data in the UART input buffer, reads it and checks for the asnwer
		answer = ptr_uart_getc();
	}
	// Waits for the asnwer with time out
	while((answer != responseExpected) && !TIME_OUT(SEND_COMMAND_TIMEOUT));
	if (answer == responseExpected)
	{
		return RECIEVE_OK;
	}
	else
	{
		return RECIEVE_ERROR;
	}
}
/*************************************************************************
Function:	unsigned char receiveString(
				uint8_t *buffer, 
				int8_t lenght, 
				int8_t indice
				)
Purpose:	Receives a string with end of message. It has a timeout of 1 
			second for the header and 1 second for the body of the message. 
Input:		uint8_t *buffer: Where the string is stored
			int8_t lenght: Max length that the string can have
			int8_t indice: Position where the buffer begins to be written
Returns:	RESULT: RECEIVE_OK or RECEIVE_ERROR
**************************************************************************/
RESULT receiveString(char *buffer, int8_t lenght, int8_t indice)
{
	uint aux;
	uint8_t fin = FALSE;
	SET_TIMEOUT_MS(UART_RX_TIMER, T0_500MS);
	do
	{
		if ( TIME_OUT(UART_RX_TIMER) == TRUE )
		fin = TRUE ;
		
		aux = UART_RXBYTE() ;
		if ( aux != UART_NO_DATA )
		{
			if (aux == END_MESSAGE)
			{
				fin = TRUE;
				*(buffer+indice) = '\0' ;
			}
			else
			{
				*(buffer+indice) = (char)aux ;
				lenght--;
				indice++;
			}
		}
		
	} while ( lenght>1 && fin == FALSE );
	if (aux == END_MESSAGE)
		return RECIEVE_OK;
	else
		return RECIEVE_ERROR;
}

/*************************************************************************
Function:	unsigned char receiveString_H(
				uint8_t *buffer, 
				int8_t lenght, 
				int8_t indice
				)
Purpose:	Receives a string with header and end of message. It has a 
			timeout of 1 second for the header and 1 second for the body 
			of the message. 
Input:		uint8_t *buffer: Where the string is stored
			int8_t lenght: Max length that the string can have
			int8_t indice: Position where the buffer begins to be written
Returns:	RESULT: RECEIVE_OK or RECEIVE_ERROR
**************************************************************************/
RESULT receiveString_H(char *buffer, int8_t lenght, int8_t indice)
{
	uint8_t header = FALSE;
	char uart_value;
	RESULT result = EMPTY ;
	
	//Recibo el comienzo del mensaje
	
	SET_TIMEOUT_MS(UART_RX_TIMER, 1000);
	do
	{
		UART_GET_CHAR(&uart_value);
		if (uart_value==START_MESSAGE)
		{
			header = TRUE ;
		}
	} while (timeOut_Timer0(UART_RX_TIMER) && header==FALSE);

	if (header==TRUE)
	{
		result = receiveString(buffer, lenght, indice);
	}
	if (header == TRUE && result == RECIEVE_OK)
	{
		return  RECIEVE_OK;
	}
	else
	{
		return  RECIEVE_ERROR;
	}
}

/*************************************************************************
Function:	unsigned char uart1_rxString(
				uint8_t *buffer, 
				int8_t lenght, 
				int8_t indice
				)
Purpose:	Receives a string with header and end of message. It has no while
			blocking loop for header. It uses receiveString for de messsage 
			body with while blocking loop.
Input:		uint8_t *buffer: Where the string is stored
			int8_t lenght: Max length that the string can have
			int8_t indice: Position where the buffer begins to be written
Returns:	RESULT: RECEIVE_OK, RECEIVE_ERROR, RECEIVE_EMPTY
**************************************************************************/
RESULT receiveString_H_noBLock(char *buffer, int8_t lenght, int8_t indice)
{
	char uart_value;
	RESULT result = EMPTY ;
	
	//Recibo el comienzo del mensaje
	
	ptr_uart_getChar(&uart_value);
	if (uart_value==START_MESSAGE)
	{
		result = receiveString(buffer, lenght, indice);
	}
	else
		return RECIEVE_EMPTY ;
	
	if (result == RECIEVE_OK)
	{
		return  RECIEVE_OK;
	}
	else
	{
		return  RECIEVE_ERROR;
	}
}

