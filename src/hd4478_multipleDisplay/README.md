avr-hd44780-lib
===============

HD44780 library for 8bit AVR


License: "Public Domain" (anything permitted), see LICENSE file

I wrote this completely from scratch because I was annoyed by incomplete LCD libraries with a restrictive or somehow incompatible license.

The documentation is included in lcd.h and lcd.c. Doxygen can be used to create a nice html documentation

### Modificacion para multiples displays

Esta modificación se realizó para el projecto Prûfstand, Versiôn Master.
Permite utilizar las mismas funciones para manipular varios displays.
Para ello se utiliza una función que setea los puertos sobre del display a 
utilizar.
