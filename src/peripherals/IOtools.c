/*
 * IOtools.c
 *
 * Created: 02/06/2017 22:08:46
 *  Author: Ezequiel Gorandi
 *  e-mail: ezequielgorandi@gmail.com
 */ 
#include <avr/io.h>
#include "../constants.h"
#include "IOtools.h"

void ioSet(uint8_t* port, uint8_t bit, uint8_t status)
{
	//DDRB |= (1<<PB7);
	*(port-1) |= (1<<bit);
	if ( status == HIGH )
	{
		*port |= (1<<bit);
		//PORTB |= (1<<PB7);
	}
	else
	{
		*port &= ~(1<<bit);
		//PORTB &= ~(1<<PB7);
	}
}

void ioToggle(uint8_t* port, uint8_t bit)
{
	//port-1 es DDRB. Como salida
	*(port-1) |= (1<<bit);
	*port = ( (*port)^(1<<bit) );
}

uint8_t readPort(volatile uint8_t* portIn, uint8_t bit )
 {
	 if ((*portIn & (1<<bit)) == 0)
		return 0 ;
	 else
		return 1 ;
 }