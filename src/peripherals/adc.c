/*
 * adc.c
 *
 * Created: 27/05/2017 14:10:25
 *  Author: Eze
 */ 
#include "adc.h"

uint16_t valorADC=0;
uint8_t flagAdc=0;
uint16_t vectorAdc[16];
uint8_t adcToRead = 0;

//*************** Inicio ADC ***************************************************
void initAdc (void)
{
    ADCSRA = 0x00; // disable adc
    ADMUX = ADC_REFERENCE_AVCC; // select adc input 0 y trabaja con la ref externa
    ADCSRA |= ADC_PRESCALER; // trabaja con un prescaler de 128. Para 16Mhz tengo t=104uS
    ACSR   = 0x80;
    ADC_ON(); 
}

//*************** Inicio ADC ***************************************************
/*void initAdcInterupt (void)
{
    ADCSRA = 0x00; // disable adc
    ADMUX = ADC_REFERENCE_AVCC; // select adc input 0 y trabaja con la ref externa
    ADCSRA |= ADC_PRESCALER; // trabaja con un prescaler de 128. Para 16Mhz tengo t=104uS
	ADCSRA |= ADC_INTERRUPT;
    ACSR   = 0x80;
    ADC_ON(); 
	ADC_START();
}
*/
//ADC initialize
// Conversion time: 104uS
void initAdcInterupt(void)
{
	ADCSRA = 0x00; //disable adc
	ADMUX  = 0x00; //select adc input 0
	ACSR   = 0x80;
	ADCSRA = 0xCF;
	ADMUX  = 0x00; //select adc input 0
}
//Lectura sin interrupciones
uint16_t readADC(uint8_t ch)
{
	//Select ADC Channel ch must be 0-7
	if (ch < 8)
	{
		ADCSRB&= ~(1<<MUX5);
	}
	else 
	{
		ADCSRB|=(1<<MUX5);
		ch = ch-8;
	}
	ch=ch&0b00000111;
	ADMUX=ch;

	//Start Single conversion
	ADC_START();

	//Wait for conversion to complete
	while(!(ADCSRA & (1<<ADIF))); //TODO: No bloqueante

	//Clear ADIF by writing one to it
	ADCSRA|=(1<<ADIF);

	return(ADC);
}

/**************************************************************************
* Prototipo: int leoADC ( uchar , uchar )
* Objetivo: Sensa N valores del canal de ADC indicado, saca el promedio y
            devuelve el valor de tipo entero, siendo el ultimo numero el
            primer decimal despues de la coma.
* Recibe:
    * Cantidad de datos para hacer el promedio (uchar),
    * Numero de canal a medir

*Devuelve:
    * Valor en mV del ADC, siendo 1024 = Vref
**************************************************************************/
uint16_t mediaAdc(uint8_t ch, uint8_t NMuestaras)
{
	uint8_t i = 0 ;
	float valor = 0 ;
	readADC(ch); // descarto primera medicion
	for (i=0; i<NMuestaras ; i++)
	{
		valor = valor+readADC(ch);
	}
	valor = valor/NMuestaras;
	valor = (ADC_REFERENCE*valor*1000/1023);
	return (uint16_t)valor;
}

uint16_t getAdcValue (uint8_t n)
{
	//vectorAdc[n] = 1250;
	return (vectorAdc[n]);
}

ISR(ADC_vect)
{			
	vectorAdc[adcToRead]=ADC;	
	adcToRead++;

	if (adcToRead>ADC_CANT)
		adcToRead = 0;
		
	if (adcToRead < 8)
	{
		ADCSRB&= ~(1<<MUX5);
		ADMUX = adcToRead;
	}
	else
	{
		ADCSRB|=(1<<MUX5);
		ADMUX = (adcToRead-8);
	}

	ADCSRA|=(1<<ADIF);
	ADC_START();
}