/**
@defgroup gorandi_timer Timer_0
@note
Configuración del timer 0.
	- 1KHz Tick
	- Normal Mode
	- Overflow interruption
*/
#include <avr/io.h>
#include <avr/interrupt.h>
#include "timerStructure.h"
#include "../../constants.h"


void init_Timer0(void);
void setTimeOut_Timer0(TIMEOUT_0 timer, unsigned int valor);
void setTimeOut_mS_Timer0(TIMEOUT_0 timer, uint16_t valor);
unsigned char timeOut_Timer0(TIMEOUT_0 timer);
void delay_Timer0(uint32_t tiempo);
void delay_ms_Timer0(uint32_t tiempo);
unsigned char getState_Timer0(TIMEOUT_0 timer);


/**************************** TIMER 0 *****************************************/

/* Base de tiempo*/
#define BASE_TIEMPO_0      1

/* Valor de los timeouts para la base de tiempo propuesta */
#define T0_1MS	   (1 / BASE_TIEMPO_0)
#define T0_10MS    (10 / BASE_TIEMPO_0)
#define T0_15MS    (15 / BASE_TIEMPO_0)
#define T0_20MS    (20 / BASE_TIEMPO_0)
#define T0_25MS    (25 / BASE_TIEMPO_0)
#define T0_30MS    (30 / BASE_TIEMPO_0)
#define T0_35MS    (35 / BASE_TIEMPO_0)
#define T0_40MS    (40 / BASE_TIEMPO_0)
#define T0_50MS    (50 / BASE_TIEMPO_0)
#define T0_55MS    (55 / BASE_TIEMPO_0)
#define T0_60MS    (60 / BASE_TIEMPO_0)
#define T0_65MS    (65 / BASE_TIEMPO_0)
#define T0_70MS    (70 / BASE_TIEMPO_0)
#define T0_75MS    (75 / BASE_TIEMPO_0)
#define T0_80MS    (80 / BASE_TIEMPO_0)
#define T0_85MS    (85 / BASE_TIEMPO_0)
#define T0_90MS    (90 / BASE_TIEMPO_0)
#define T0_95MS    (95 / BASE_TIEMPO_0)
#define T0_100MS   (100 / BASE_TIEMPO_0)
#define T0_105MS   (105 / BASE_TIEMPO_0)
#define T0_110MS   (110 / BASE_TIEMPO_0)
#define T0_115MS   (115 / BASE_TIEMPO_0)
#define T0_125MS   (125 / BASE_TIEMPO_0)
#define T0_130MS   (130 / BASE_TIEMPO_0)
#define T0_135MS   (135 / BASE_TIEMPO_0)
#define T0_140MS   (140 / BASE_TIEMPO_0)
#define T0_150MS   (150 / BASE_TIEMPO_0)
#define T0_155MS   (155 / BASE_TIEMPO_0)
#define T0_160MS   (160 / BASE_TIEMPO_0)
#define T0_165MS   (165 / BASE_TIEMPO_0)
#define T0_170MS   (170 / BASE_TIEMPO_0)
#define T0_175MS   (175 / BASE_TIEMPO_0)
#define T0_180MS   (180 / BASE_TIEMPO_0)
#define T0_185MS   (185 / BASE_TIEMPO_0)
#define T0_190MS   (190 / BASE_TIEMPO_0)
#define T0_195MS   (195 / BASE_TIEMPO_0)
#define T0_200MS   (200 / BASE_TIEMPO_0)
#define T0_225MS   (225 / BASE_TIEMPO_0)
#define T0_250MS   (250 / BASE_TIEMPO_0)
#define T0_275MS   (275 / BASE_TIEMPO_0)
#define T0_300MS   (300 / BASE_TIEMPO_0)
#define T0_325MS   (325 / BASE_TIEMPO_0)
#define T0_350MS   (350 / BASE_TIEMPO_0)
#define T0_375MS   (375 / BASE_TIEMPO_0)
#define T0_400MS   (400 / BASE_TIEMPO_0)
#define T0_425MS   (425 / BASE_TIEMPO_0)
#define T0_450MS   (450 / BASE_TIEMPO_0)
#define T0_475MS   (475 / BASE_TIEMPO_0)
#define T0_500MS   (500 / BASE_TIEMPO_0)
#define T0_550MS   (550 / BASE_TIEMPO_0)
#define T0_800MS   (800 / BASE_TIEMPO_0)
#define T0_900MS   (900 / BASE_TIEMPO_0)
#define T0_1S      (1000 / BASE_TIEMPO_0)
#define T0_2S      (2000 / BASE_TIEMPO_0)
#define T0_3S      (3000 / BASE_TIEMPO_0)
#define T0_5S      (5000 / BASE_TIEMPO_0)
#define T0_8S      (8000 / BASE_TIEMPO_0)
#define T0_10S     (10000 / BASE_TIEMPO_0)
#define T0_60S      (60000 /(int32_t)BASE_TIEMPO_0)



// Timer 0 Configuration
//	1KHz Tick
//	Normal Mode
//#define TIMER0_COUNT  0x64 //
//#define TIMER0_PRESCALER  0x05 //< Configuración del prescaler
/*
#define TIMER0_COUNT  0x06 //
#define TIMER0_PRESCALER  0x03 //< Configuración del prescaler
*/


#define TIMER0_COUNT  0x06 //
#define TIMER0_PRESCALER  0x03 //< Configuración del prescaler


#define TIMER0_TCCR0A 0x00 //< OCCR Disconneted
#define TIMER0_TCCR0B 0x00 //< No PWM Mode
#define TIMER0_ON (TIMER0_TCCR0B | TIMER0_PRESCALER) //< Start Timer
#define TIMER0_OFF (TIMER0_TCCR0B & ~TIMER0_PRESCALER)
#define TIMER0_SYNC_INTERNAL_MODE 0x00
#define TIMER0_INTERRUPT_ON TIMSK0 | (1<<TOIE0) //< Enable Timer Interrupt

/**************************** FIN TIMER 0 *************************************/

/**@{*/