/*
 * timer1.c
 *
 * Created: 20/02/2017 07:08:10 p. m.
 *  Author: Ezequiel
 */ 
/**
@defgroup gorandi_timer Timer_0
@date 12/12/2016.
@author Ezequiel Gorandi
@version 1.0
@code
#include "timer1.h"
@endcode
@brief
Librer�a para la utilizaci�n del timer 1 del uC Atmega2560
*/


/**@{*/

#define _timer1_
#include "timer1.h"

TIMERS_INTERNOS Timers_1[CANT_TIMER_1];
time_t timestamp = 0;

/**
@brief   Atenci�n de la interrupci�n por overflow del timer 1.
@note    
   - Actualiza los timeouts definidos.
   - Recarga la cuenta del timer 1
*/
ISR(TIMER1_OVF_vect)
{
	uint8_t i;
	/* recargo las cuentas */
	TCNT1H = TIMER1_COUNT_H; //Set Count
	TCNT1L = TIMER1_COUNT_L;
	
	//-- Systick
	system_tick();
	
	
	/* decremento los timeouts */
	for (i = 0; i < CANT_TIMER_1; i++)
		if(Timers_1[i].Tiempo) Timers_1[i].Tiempo--;
}


/**
@brief   Inicializaci�n del timer 1.
@param   void
@return  void
@note    Configura el timer con los par�metros	definidos en timero.h
*/
void init_Timer1(void)
{

   uint8_t i;

   TCCR1B = TIMER1_TCCR1B; //Timer 1 Stop
   ASSR  = TIMER1_SYNC_INTERNAL_MODE; //Sync mode. Internal Clock
   TCNT1H = TIMER1_COUNT_H; //Set Count
   TCNT1L = TIMER1_COUNT_L;
   TIMSK1 = TIMER1_INTERRUPT_ON; //Enable Timer Interrupt

// All the Timer1 TimeOut = 0
   for(i = 0; i < CANT_TIMER_1; i++) 
   {
      Timers_1[i].Tiempo = 0;
      Timers_1[i].En_uso = 0;
   }
   
   TCCR1B = TIMER1_ON; //Start Timer
}

/**
@brief   Carga un valor determinado en un timerout.
@param   uint8_t : timeout que se desea settear.
@param   uint8_t : valor con el cual desea settearse el timeout.
@return  void
@note    Los timeouts utilizados por el programa, as� como los valores con los
         que estos pueden ser setteados, son definidos en timer1.h
*/
void setTimeOut_Timer1(uint8_t timer, uint16_t valor)
{
   /* cargo el valor de tiempo */
   Timers_1[timer].Tiempo = valor;
   /* activo el uso del timer */
   Timers_1[timer].En_uso = TRUE;
}
/**
@brief   Verifica si se ha vencido un determinado timeout
@param   uint8_t : timeout que se desea verificar.
@retval  uint8_t TRUE : Hubo timeout
@retval  uint8_t FALSE : A�n no hubo timeout
@note    
   - Los timeouts utilizados por el programa, son definidos en timer1.h
   - Previamente debe settearse el timeout utilizando 
     @codevoid setTimeOut_Timer1(uint8_t timer, uint16_t valor)@endcode
   - Una vez ocurrido el timeout, debe settearse nuevamente esta funcion si se
     quiere que vuelva a ocurrir.
*/
uint8_t timeOut_Timer1(uint8_t timer)
{
   if(Timers_1[timer].En_uso && !Timers_1[timer].Tiempo)
   {
      Timers_1[timer].En_uso = FALSE;
      return TRUE;
   }
   return FALSE;
}

time_t getTimestamp ()
{
	return timestamp ;
}

void setTimeOut_S_Timer1(TIMEOUT_1 timer, uint16_t valor)
{
	valor = valor/BASE_TIEMPO_1;
	/* cargo el valor de tiempo */
	Timers_1[timer].Tiempo = valor;
	/* activo el uso del timer */
	Timers_1[timer].En_uso = TRUE;
}