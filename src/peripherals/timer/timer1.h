/**
@defgroup gorandi_timer Timer_1
@note
Configuraci󮠤el timer 1.
	- 1KHz Tick
	- Normal Mode
	- Overflow interruption
*/

/**@{*/

#include "timerStructure.h"
#include "time.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include "../../constants.h"

/* ----------------------------------------------------------------------- */
#ifdef _timer1_   
/* ----------------------------------------------------------------------- */

void init_Timer1(void);
void setTimeOut_Timer1(uchar timer, uint valor);
uchar timeOut_Timer1(uchar timer);
void delay_Timer1(uint tiempo);
time_t getTimestamp (void);
void setTimeOut_S_Timer1(TIMEOUT_1 timer, uint16_t valor);
/* ----------------------------------------------------------------------- */
#else
/* ----------------------------------------------------------------------- */
extern void setTimeOut_S_Timer1(TIMEOUT_1 timer, uint16_t valor);
extern void init_Timer1(void);
extern void setTimeOut_Timer1(uchar timer, uint valor);
extern uchar timeOut_Timer1(uchar timer);
extern void delay_Timer1(uint tiempo);
time_t getTimestamp (void);
/* ----------------------------------------------------------------------- */
#endif
/* ----------------------------------------------------------------------- */

/**************************** TIMER 1 *****************************************/

/* Base de tiempo*/
#define BASE_TIEMPO_1      1 //

/* Valor de los timeouts para la base de tiempo propuesta */
#define T1_1S      (1 /BASE_TIEMPO_1)
#define T1_2S      (2 /BASE_TIEMPO_1)
#define T1_5S      (5 /BASE_TIEMPO_1)
#define T1_10S      (10 /BASE_TIEMPO_1)
#define T1_15S      (15 /BASE_TIEMPO_1)
#define T1_20S      (20 /BASE_TIEMPO_1)
#define T1_30S      (30 /BASE_TIEMPO_1)
#define T1_35S      (35 /BASE_TIEMPO_1)
#define T1_45S      (45 /BASE_TIEMPO_1)
#define T1_50S      (50 /BASE_TIEMPO_1)
#define T1_55S      (55 /BASE_TIEMPO_1)
#define T1_60S      (60 /BASE_TIEMPO_1)
#define T1_90S      (90 /BASE_TIEMPO_1)
#define T1_1M      (60 /BASE_TIEMPO_1)
#define T1_4M      (240 /BASE_TIEMPO_1)
#define T1_5M      (300 /BASE_TIEMPO_1)
#define T1_1H      (3600 /BASE_TIEMPO_1)



// Timer 1 Configuration
//	1KHz Tick
//	Normal Mode
#define TIMER1_COUNT_H  0xC2
#define TIMER1_COUNT_L  0xF7
#define TIMER1_PRESCALER  0x05 //< Configuraci󮠤el prescaler
#define TIMER1_TCCR1A 0x00 //< OCCR Disconneted
#define TIMER1_TCCR1B 0x00 //< No PWM Mode
#define TIMER1_ON (TIMER1_TCCR1B | TIMER1_PRESCALER) //< Start Timer
#define TIMER1_OFF (TIMER1_TCCR1B & ~TIMER1_PRESCALER)
#define TIMER1_SYNC_INTERNAL_MODE 0x00
#define TIMER1_INTERRUPT_ON TIMSK1 | (1<<TOIE1) //< Enable Timer Interrupt

/**************************** FIN TIMER 0 *************************************/

/**@{*/