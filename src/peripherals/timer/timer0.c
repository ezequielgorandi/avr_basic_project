/**
@defgroup gorandi_timer Timer_0
@date 12/12/2016.
@author Ezequiel Gorandi
@version 1.0
@code
#include "timer0.h"
@endcode
@brief
Librería para la utilización del timer 0 del uC Atmega2560
*/


/**@{*/

#define _timer0_
#include "timer0.h"


TIMERS_INTERNOS Timers_0[CANT_TIMER_0];

uint8_t TECLA_NUEVA = FALSE;	   //flag
uint8_t buff_fila1 = 0, buff_fila2 = 0, buff_fila3 =0, buff_fila4 = 0;
uint8_t buff_teclado = 0;
uint8_t debounce;
uint8_t tecla_actual;
uint8_t tecla_anterior;

uint16_t timeraux = 0; 


/**
@brief   Atención de la interrupción por overflow del timer 0.
@note    
   - Actualiza los timeouts definidos.
   - Recarga la cuenta del timer 0
*/
ISR(TIMER0_OVF_vect)
{
	
	uint8_t i;
	/* recargo las cuentas */
	TCNT0 = TIMER0_COUNT;
	/* decremento los timeouts */
	for (i = 0; i < CANT_TIMER_0; i++)
		if(Timers_0[i].Tiempo) Timers_0[i].Tiempo--;
}


/**
@brief   Inicialización del timer 0.
@param   void
@return  void
@note    Configura el timer con los parámetros definidos en timero.h
*/
void init_Timer0(void)
{
   uint8_t i;

   TCCR0A = TIMER0_TCCR0A; //Timer 0 Stop
   ASSR  = TIMER0_SYNC_INTERNAL_MODE; //Sync mode. Internal Clock
   TCNT0 = TIMER0_COUNT; //Set Count
   TIMSK0 = TIMER0_INTERRUPT_ON; //Enable Timer Interrupt

// All the Timer0 TimeOut = 0
   for(i = 0; i < CANT_TIMER_0; i++) 
   {
      Timers_0[i].Tiempo = 0;
      Timers_0[i].En_uso = 0;
   }
   
   TCCR0B = TIMER0_ON; //Start Timer
}


void setTimeOut_mS_Timer0(TIMEOUT_0 timer, uint16_t valor)
{
	valor = valor/BASE_TIEMPO_0; 
	/* cargo el valor de tiempo */
	Timers_0[timer].Tiempo = valor;
	/* activo el uso del timer */
	Timers_0[timer].En_uso = TRUE;
}

/**
@brief   Carga un valor determinado en un timerout.
@param   uint8_t : timeout que se desea settear.
@param   uint8_t : valor con el cual desea settearse el timeout.
@return  void
@note    Los timeouts utilizados por el programa, así como los valores con los
         que estos pueden ser setteados, son definidos en timer0.h
*/
void setTimeOut_Timer0(TIMEOUT_0 timer, uint16_t valor)
{
   /* cargo el valor de tiempo */
   Timers_0[timer].Tiempo = valor;
   /* activo el uso del timer */
   Timers_0[timer].En_uso = TRUE;
}
/**
@brief   Verifica si se ha vencido un determinado timeout
@param   uint8_t : timeout que se desea verificar.
@retval  uint8_t TRUE : Hubo timeout
@retval  uint8_t FALSE : Aún no hubo timeout
@note    
   - Los timeouts utilizados por el programa, son definidos en timer0.h
   - Previamente debe settearse el timeout utilizando 
     @codevoid setTimeOut_Timer0(uint8_t timer, uint16_t valor)@endcode
   - Una vez ocurrido el timeout, debe settearse nuevamente esta funcion si se
     quiere que vuelva a ocurrir.
*/
uint8_t timeOut_Timer0(TIMEOUT_0 timer)
{
   if(Timers_0[timer].En_uso && !Timers_0[timer].Tiempo)
   {
      Timers_0[timer].En_uso = FALSE;
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************/
uint8_t getState_Timer0(TIMEOUT_0 timer)
{
	return Timers_0[timer].En_uso;
}

/**************************************************************************
 Prototipo: void Delay_0(uint16_t tiempo)
 Objetivo:  esperar el tiempo especificado
 Recibe:    tiempo
 Devuelve:  
 Notas:
 **************************************************************************/
void delay_Timer0(uint32_t tiempo)
{
   /* cargo el tiempo que me pidieron */
   setTimeOut_Timer0(DELAY_0, tiempo);
   /* espero a que pase */
   while(!timeOut_Timer0(DELAY_0))asm("wdr"); 
}
/**************************************************************************/


/**************************************************************************
 Prototipo: void Delay_0(uint16_t tiempo)
 Objetivo:  esperar el tiempo especificado
 Recibe:    tiempo
 Devuelve:  
 Notas:
 **************************************************************************/
void delay_ms_Timer0(uint32_t tiempo)
{
   /* cargo el tiempo que me pidieron */
   setTimeOut_mS_Timer0(DELAY_0, tiempo);
   /* espero a que pase */
   while(!timeOut_Timer0(DELAY_0))asm("wdr"); 
}
/**************************************************************************/
/**@{*/