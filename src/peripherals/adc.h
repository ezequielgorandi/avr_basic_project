/*
 * adc.h
 *
 * Created: 28/05/2017 19:54:22
 *  Author: Eze
 */ 
#include <avr/io.h>
#include <avr/interrupt.h>

#define ADC_PRESCALER 0x07
#define ADC_INTERRUPT (1<<ADIF)
#define ADC_REFERENCE_AREF 0x00
#define ADC_REFERENCE_AVCC 0x40
#define ADC_REFERENCE_INTERNAL_1_1 0x80
#define ADC_REFERENCE_INTERNAL_2_56 0xC0
#define ADC_REFERENCE 5

#define ADC_ON() ADCSRA|=(1<<ADEN) //prendo el ADC;
#define ADC_OFF() ADCSRA&=~(1 << ADEN); //apago el ADC;
#define ADC_START() ADCSRA|=(1<<ADSC) //Start;
#define ADC_STOP() ADCSRA&=~(1<<ADSC) //Stop;
#define ADC_CANT 10


uint16_t lecturaAdc ( uint8_t, uint8_t );

void initAdc();

//ADC initialize
// Conversion time: 104uS
void initAdcInterupt(void);
//Lectura sin interrupciones
uint16_t readADC(uint8_t ch);
uint16_t mediaAdc(uint8_t ch, uint8_t NMuestaras);
uint16_t getAdcValue (uint8_t n);