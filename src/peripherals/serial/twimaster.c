/*************************************************************************
* Title:    I2C master library using hardware TWI interface
* Author:   Peter Fleury <pfleury@gmx.ch>  http://jump.to/fleury
* File:     $Id: twimaster.c,v 1.4 2015/01/17 12:16:05 peter Exp $
* Software: AVR-GCC 3.4.3 / avr-libc 1.2.3
* Target:   any AVR device with hardware TWI 
* Usage:    API compatible with I2C Software Library i2cmaster.h
**************************************************************************/
/**
 * @date:   9/4/2018
 * @version: 2.00
 * @author: Ezequiel Gorandi
 * @email:  ezequielgorandi@gmail.com
 * @brief: 
 *	BuxFix: Se agregan timeouts para evitar que el software se
	freeze si hay problemas con alg�n sensor. ATENCI�N: ES NECESARIO
	DEFINIR EL TIMEOUT EN EL TIMER0
 *	BugFix: Se agrega un reseteo de la l�nea en la funci�n i2c_start();	 
	Description: La l�nea I2C se freeza (SCL=HIGH, SDA=HIGH) cuando, por ejemplo,
	se conecta un sensor sin alimentaci�n. Esto imped�a medir cualquier otro 
	componente I2C. Al resetear la linea I2C se resuelve el problema.
 */

#include <inttypes.h>
#include <compat/twi.h>

#include "i2cmaster.h"
#include "../timer/timer0.h"

/* define CPU frequency in hz here if not defined in Makefile */
#ifndef F_CPU
#define F_CPU 16000000UL
#endif

/* I2C clock in Hz */
#define SCL_CLOCK  100000L



/**
 * @date:   9/4/2018
 * @author: Ezequiel Gorandi
 * @email:  ezequielgorandi@gmail.com
 * @brief:  timeout waiting for the stop_condotion.
 * @note:   Needs timeout implementation in timer 0.
 * @return: void 
 */
void wait_stop_condition()
{
 	setTimeOut_Timer0(I2C_TIMEOUT,T0_300MS);
 	while(TWCR & (1<<TWSTO) &&!timeOut_Timer0(I2C_TIMEOUT));
 	return;
}

/**
 * @date:   9/4/2018
 * @author: Ezequiel Gorandi
 * @email:  ezequielgorandi@gmail.com
 * @brief:  timeout waiting for the transmition to complete.
 * @note:   Needs timeout implementation in timer 0.
 * @return: void 
 */
void wait_transmition_complete()
{
  	setTimeOut_Timer0(I2C_TIMEOUT,T0_300MS);
  	while( !(TWCR & (1<<TWINT)) && !timeOut_Timer0(I2C_TIMEOUT));
  	return;
}

/*************************************************************************
 Initialization of the I2C bus interface. Need to be called only once
*************************************************************************/
void i2c_init(void)
{
  /* initialize TWI clock: 100 kHz clock, TWPS = 0 => prescaler = 1 */
  TWSR = 0;                         /* no prescaler */
  TWBR = ((F_CPU/SCL_CLOCK)-16)/2;  /* must be > 10 for stable operation */

}/* i2c_init */


/*************************************************************************	
  Issues a start condition and sends address and transfer direction.
  return 0 = device accessible, 1= failed to access device
*************************************************************************/
unsigned char i2c_start(unsigned char address)
{
    uint8_t   twst;
	
	TWCR &= ~(_BV(TWEN)); //NEW!! 09/04/2018. This turn off the i2c. Avoid i2c hang
	
	// send START condition
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);

	// wait until transmission completed
	wait_transmition_complete();

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_START) && (twst != TW_REP_START)) return 1;

	// send device address
	TWDR = address;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wail until transmission completed and ACK/NACK has been received
	wait_transmition_complete();

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) ) return 1;

	return 0;

}/* i2c_start */


/*************************************************************************
 Issues a start condition and sends address and transfer direction.
 If device is busy, use ack polling to wait until device is ready
 
 Input:   address and transfer direction of I2C device
*************************************************************************/
//TODO: Revisar
void i2c_start_wait(unsigned char address)
{
    uint8_t   twst;
    while ( 1 )
	#ifdef _timer0_
	setTimeOut_Timer0(I2C_TIMER2, T0_500MS);
	#endif
/*	
	#ifdef _timer0_
    while (  timeOut_Timer0(I2C_TIMER2) == FALSE ) 
	#else
	while()
	#endif
	*/

	while (
		#ifdef _timer0_ 
			timeOut_Timer0(I2C_TIMER2) == FALSE 
		#else 
			1
		#endif	 
		)	
    {
	    // send START condition
	    TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
    
    	// wait until transmission completed
    	wait_transmition_complete();
    
    	// check value of TWI Status Register. Mask prescaler bits.
    	twst = TW_STATUS & 0xF8;
    	if ( (twst != TW_START) && (twst != TW_REP_START)) continue;
    
    	// send device address
    	TWDR = address;
    	TWCR = (1<<TWINT) | (1<<TWEN);
    
    	// wail until transmission completed
    	wait_transmition_complete(); //TODO: Funcion Bloqueante
    
    	// check value of TWI Status Register. Mask prescaler bits.
    	twst = TW_STATUS & 0xF8;
    	if ( (twst == TW_MT_SLA_NACK )||(twst ==TW_MR_DATA_NACK) ) 
    	{    	    
    	    /* device busy, send stop condition to terminate write operation */
	        TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
	        
	        // wait until stop condition is executed and bus released
	        wait_stop_condition();   
    	    continue;
    	}
    	//if( twst != TW_MT_SLA_ACK) return 1;
    	break;
     }

}/* i2c_start_wait */


/*************************************************************************
 Issues a repeated start condition and sends address and transfer direction 

 Input:   address and transfer direction of I2C device
 
 Return:  0 device accessible
          1 failed to access device
*************************************************************************/
unsigned char i2c_rep_start(unsigned char address)
{
    return i2c_start( address );

}/* i2c_rep_start */


/*************************************************************************
 Terminates the data transfer and releases the I2C bus
*************************************************************************/
void i2c_stop(void)
{
    /* send stop condition */
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
	
	// wait until stop condition is executed and bus released
	wait_stop_condition();

}/* i2c_stop */


/*************************************************************************
  Send one byte to I2C device
  
  Input:    byte to be transfered
  Return:   0 write successful 
            1 write failed
*************************************************************************/
unsigned char i2c_write( unsigned char data )
{	
    uint8_t   twst;
    
	// send data to the previously addressed device
	TWDR = data;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wait until transmission completed
	wait_transmition_complete();

	// check value of TWI Status Register. Mask prescaler bits
	twst = TW_STATUS & 0xF8;
	if( twst != TW_MT_DATA_ACK) return 1;
	return 0;

}/* i2c_write */


/*************************************************************************
 Read one byte from the I2C device, request more data from device 
 
 Return:  byte read from I2C device
*************************************************************************/
unsigned char i2c_readAck(void)
{
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);
	wait_transmition_complete();    

    return TWDR;

}/* i2c_readAck */


/*************************************************************************
 Read one byte from the I2C device, read is followed by a stop condition 
 
 Return:  byte read from I2C device
*************************************************************************/
unsigned char i2c_readNak(void)
{
	TWCR = (1<<TWINT) | (1<<TWEN);
	wait_transmition_complete();
	
    return TWDR;

}/* i2c_readNak */



int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len)
{
    int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */

    /*
     * The parameter dev_id can be used as a variable to store the I2C address of the device
     */

    /*
     * Data on the bus should be like
     * |------------+---------------------|
     * | I2C action | Data                |
     * |------------+---------------------|
     * | Start      | -                   |
     * | Write      | (reg_addr)          |
     * | Stop       | -                   |
     * | Start      | -                   |
     * | Read       | (reg_data[0])       |
     * | Read       | (....)              |
     * | Read       | (reg_data[len - 1]) |
     * | Stop       | -                   |
     * |------------+---------------------|
     */

    return rslt;
}
