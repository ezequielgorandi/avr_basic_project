/*
 * serialComunication.h
 *
 * Created: 26/09/2017 20:28:12
 *  Author: Ezequiel Gorandi
 *  e-mail: ezequielgorandi@gmail.com
 */ 

#include <avr/io.h>
#include "peripherals/timer/timer0.h"
#include "peripherals/serial/uart.h"

#ifndef SERIALCOMUNICATION_H_
#define SERIALCOMUNICATION_H_

#define START_MESSAGE '$'
#define END_MESSAGE '/'
#define SEPARADOR_ATRIBUTOS ','
#define FIN_DE_CADENA '\0'

#define UART_TXBYTE ptr_uart_putc//uart1_putc
#define UART_RXBYTE ptr_uart_getc//uart1_getc
#define UART_TXSTRING ptr_uart_puts//uart1_puts
#define UART_GET_CHAR ptr_uart_getChar
//#define UART_DATO_RX_BUF uart1_datoEnRxBuf

#define SET_TIMEOUT_MS setTimeOut_mS_Timer0
#define TIME_OUT timeOut_Timer0

#endif /* SERIALCOMUNICATION_H_ */

typedef enum 
{
	RECIEVE_OK = 0,
	RECIEVE_ERROR,
	RECIEVE_EMPTY,
	EMPTY
}RESULT; 

/**
 * @brief [brief description]
 * @details [long description]
 * 
 * @param aCommand [La respuesta esperada]
 * @param aResponse [description]
 * @param timeout [description]
 * @return [description]
 */
void comConfig (unsigned int (*aPtr_uart_getc) (void),
				unsigned char (*aPtr_uart_getChar)(char *),
				void (*aPtr_uart_puts)(const char *),
				void (*aPtr_uart_putc)(unsigned char )
				) ;
void confUart_getChar (char * uart_value);
RESULT sendComand(uint8_t aCommand, uint8_t aResponse, uint16_t timeout);
RESULT sendComandString(char * aCommand, uint8_t aResponse, uint16_t timeout);
//RESULT recieveCommand (uint8_t aCommand);
/** @brief Receives a string with header and end of message. */
RESULT receiveString_H(char *buffer, int8_t lenght, int8_t indice);
RESULT receiveString(char *buffer, int8_t lenght, int8_t indice);
RESULT receiveString_H_noBLock(char *buffer, int8_t lenght, int8_t indice);


