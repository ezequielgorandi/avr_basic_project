/*
 * stringTools.c
 *
 * Created: 24/03/2017 02:19:12 p. m.
 *  Author: Ezequiel
 */ 
#include <avr/io.h>

/**
 @brief Escribe un string en un buffer.
 @param char*. El string que se desea copiar en el buffer
 @param char*. Bufer donde se guarda el string
 @param uint_8_t. La longitud del string que se va a escribit en el buffer.
 @param uint_8_t. El �ndice indica la posici�n del buffer donde se debe escribir
				  el string
 @return uint8_t. Devuelve la posici�n donde se termin� de escribir el string.
 @note 
*/
uint8_t writeStringOnBufferInv(char * stringAux, char*buffer ,uint8_t lenght, uint8_t indice)
{
	int i ;
	for (i=0 ; i<=lenght-1 ; i++ , indice++)
	{
		*(buffer+indice)=*(stringAux+i);
	}
	return indice;
}

uint8_t writeCharOnBuffer(char character, char*buffer , uint8_t indice)
{
	*(buffer+indice)=character;
	indice++;
	return indice;
}


/**
 @brief Escribe un string en un buffer.
 @param char*. El string que se desea copiar en el buffer
 @param char*. Bufer donde se guarda el string
 @param uint_8_t. La longitud del string que se va a escribit en el buffer.
 @param uint_8_t. El �ndice indica la posici�n del buffer donde se debe escribir
				  el string
 @return uint8_t. Devuelve la posici�n donde se termin� de escribir el string.
 @note 
*/
uint8_t writeStringOnBuffer(char * stringAux, char*buffer ,uint8_t lenght, uint8_t indice)
{
	int i ;
	for (i=lenght-1 ; i>=0 ; i-- , indice++)
	{
		*(buffer+indice)=*(stringAux+i);
	}
	return indice;
}