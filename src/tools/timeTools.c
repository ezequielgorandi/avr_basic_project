/*
 * timeTools.c
 *
 * Created: 10/11/2017 12:04:34 p.m.
 *  Author: Ezequiel
 */ 
#include <avr/io.h>
#include <time.h>
#include <string.h>
/**
 @brief Transforma la fecha de timeStruct en un string del tipo DDMMAA
 @param struct tm*. Estructura time
 @param char*. Bufer donde se guarda el string
 @return char. Devuelve 0.
 @note 
	 El main consta de una serie de timeouts que definen la frecuencia de
	 toma de datos de cada uno de los sensores, como as� tambi�n la frecuencia
	 de actualizaci�n del display LCD.
	 Tener en cuenta a la hora de definir los timeouts, los tiempos de conversi�n
	 de cada uno de los sensores.
*/
#include "stringTools.h"
#include "asciiTools.h"

/**
 * @date:   16/01/2018
 * @author: Ezequiel Gorandi
 * @email:  ezequielgorandi@gmail.com
 * 
 * @brief: 
 * 
 * @param:  struct tm * timeStruct 
 * @param:  char * buffer 
 * 
 * @return: char 
 */
char getStringDate( struct tm  *timeStruct, char* buffer)
{
	char stringAux[3];
	uint8_t indice = 0 ;
	itoa(timeStruct->tm_year-130, stringAux,10);
	indice = writeStringOnBuffer(stringAux, buffer ,strlen(stringAux), indice);
	intToStr(timeStruct->tm_mon+1, stringAux,2);
	indice = writeStringOnBuffer(stringAux, buffer ,strlen(stringAux), indice);
	intToStr(timeStruct->tm_mday, stringAux,2);
	indice = writeStringOnBuffer(stringAux, buffer ,strlen(stringAux), indice);
	indice = writeStringOnBuffer('\0', buffer ,1, indice);
	return 0 ;
}

/**
 @brief Transforma el horario de timeStruct en un string del tipo HHMMSS
 @param struct tm*. Estructura time
 @param char*. Bufer donde se guarda el string
 @return char. Devuelve 0.
 @note 
*/
char getStringTime( struct tm *timeStruct, char* buffer)
{
	char stringAux[3];
	uint8_t indice = 0 ;

	intToStr(timeStruct->tm_sec, stringAux,2);
	indice = writeStringOnBuffer(stringAux, buffer ,strlen(stringAux), indice);
	intToStr(timeStruct->tm_min, stringAux,2);
	indice = writeStringOnBuffer(stringAux, buffer ,strlen(stringAux), indice);
	intToStr(timeStruct->tm_hour, stringAux,2);
	indice = writeStringOnBuffer(stringAux, buffer ,strlen(stringAux), indice);

	indice = writeStringOnBuffer('\0', buffer ,1, indice);
	return 0 ;
}


char getStringTime_fromTimestamp( time_t *timestamp, char* buffer)
{
	struct tm * timeStruct ;
	timeStruct = gmtime(timestamp);
	getStringTime( timeStruct, buffer);

	return 0 ;
}

/**
 @brief Transforma el horario de timeStruct en un string del tipo HHMMSS
 @param struct tm*. Estructura time
 @param char*. Bufer donde se guarda el string
 @return char. Devuelve 0.
 @note 
*/
char getStringDate_fromTimestamp( time_t *timestamp, char* buffer)
{
	struct tm * timeStruct ;
	timeStruct = gmtime(timestamp);
	getStringDate( timeStruct, buffer);
	return 0 ;
}