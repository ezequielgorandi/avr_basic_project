/*
 * asciiTools.c
 *
 * Created: 16/01/2017 08:38:56 p. m.
 *  Author: Ezequiel
 *  Descripción General: 
 *  	Funciones para realizar la conversión de número binarios a códigos ASCII para
 *  	poder ser impresos.
 * 
 *  Consideraciones:
 *  	¡Atención! Las funciones utilizan buffers de distintos tamaños, que deben ser 
 *  	correctamente declarados por el usuario.
 */ 

/*Signado*/
#include <avr/io.h>
#include <stdlib.h>

void uint32ToAscii (uint32_t bin, char* Digitos)
{
	unsigned char sal1,sal2, sal3, sal4, sal5, sal6, sal7, sal8, sal9, sal10;

	sal10 = (bin / 1000000000);
	*(Digitos + 9) = sal10+48;
	sal9 = ((bin - (sal10 * 1000000000)) / 100000000);
	*(Digitos + 8) = sal9 + 48;
	sal8 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000)) / 10000000);
	*(Digitos + 7) = sal8 + 48;
	sal7 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000)) / 1000000);
	*(Digitos + 6) = sal7 + 48;
	sal6 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000)) / 100000);
	*(Digitos + 5) = sal6 + 48;
	sal5 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000)) / 10000);
	*(Digitos + 4) = sal5 + 48;
	sal4 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000) - (sal5 * (uint32_t)10000)) / 1000);
	*(Digitos + 3) = sal4 + 48;
	sal3 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000) - (sal5 * (uint32_t)10000) - (sal4 * 1000)) / 100);
	*(Digitos + 2) = sal3 + 48;
	sal2 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000) - (sal5 * (uint32_t)10000) - (sal4 * 1000) - (sal3 * 100)) / 10);
	*(Digitos + 1) = sal2 + 48;
	sal1 = (bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000) - (sal5 * (uint32_t)10000) - (sal4 * 1000) - (sal3 * 100) - (sal2 * 10));
	*(Digitos) = sal1 + 48;
	asm("wdr");
}

void uint32ToAsciiInv (uint32_t bin, char* Digitos)
{
	unsigned char sal1,sal2, sal3, sal4, sal5, sal6, sal7, sal8, sal9, sal10;

	sal10 = (bin / 1000000000);
	*(Digitos) = sal10+48;
	sal9 = ((bin - (sal10 * 1000000000)) / 100000000);
	*(Digitos + 1) = sal9 + 48;
	sal8 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000)) / 10000000);
	*(Digitos + 2) = sal8 + 48;
	sal7 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000)) / 1000000);
	*(Digitos + 3) = sal7 + 48;
	sal6 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000)) / 100000);
	*(Digitos + 4) = sal6 + 48;
	sal5 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000)) / 10000);
	*(Digitos + 5) = sal5 + 48;
	sal4 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000) - (sal5 * (uint32_t)10000)) / 1000);
	*(Digitos + 6) = sal4 + 48;
	sal3 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000) - (sal5 * (uint32_t)10000) - (sal4 * 1000)) / 100);
	*(Digitos + 7) = sal3 + 48;
	sal2 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000) - (sal5 * (uint32_t)10000) - (sal4 * 1000) - (sal3 * 100)) / 10);
	*(Digitos + 8) = sal2 + 48;
	sal1 = (bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000) - (sal5 * (uint32_t)10000) - (sal4 * 1000) - (sal3 * 100) - (sal2 * 10));
	*(Digitos+9) = sal1 + 48;
	asm("wdr");
}

/*Signado*/
void int32ToAscii (int32_t bin, char* Digitos)
{
	unsigned char sal1,sal2, sal3, sal4, sal5, sal6, sal7, sal8, sal9, sal10;
	if (bin<0)
	{
		bin = bin*(-1);
		*(Digitos + 10)='-';
	}
	else
	*(Digitos + 10)=' ';
	
	sal10 = (bin / 1000000000);
	*(Digitos + 9) = sal10+48;
	sal9 = ((bin - (sal10 * 1000000000)) / 100000000);
	*(Digitos + 8) = sal9 + 48;
	sal8 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000)) / 10000000);
	*(Digitos + 7) = sal8 + 48;
	sal7 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000)) / 1000000);
	*(Digitos + 6) = sal7 + 48;
	sal6 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000)) / 100000);
	*(Digitos + 5) = sal6 + 48;
	sal5 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000)) / 10000);
	*(Digitos + 4) = sal5 + 48;
	sal4 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000) - (sal5 * (uint32_t)10000)) / 1000);
	*(Digitos + 3) = sal4 + 48;
	sal3 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000) - (sal5 * (uint32_t)10000) - (sal4 * 1000)) / 100);
	*(Digitos + 2) = sal3 + 48;
	sal2 = ((bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000) - (sal5 * (uint32_t)10000) - (sal4 * 1000) - (sal3 * 100)) / 10);
	*(Digitos + 1) = sal2 + 48;
	sal1 = (bin - (sal10 * 1000000000) - (sal9 * 100000000) - (sal8 * 10000000) - (sal7 * 1000000) - (sal6 * 100000) - (sal5 * (uint32_t)10000) - (sal4 * 1000) - (sal3 * 100) - (sal2 * 10));
	*(Digitos) = sal1 + 48;
	asm("wdr");
}

/*Signado*/
void intToAscii (int bin, char* Digitos)
{
	unsigned char sal2, sal3, sal4, sal5;

	if (bin >= 0)
	{
		sal5 = (bin / 10000);
		*(Digitos + 4) = ' ';
		sal4 = ((bin - (sal5 * 10000)) / 1000);
		*(Digitos + 3) = sal4 + 48;
		sal3 = ((bin - (sal5 * 10000) - (sal4 * 1000)) / 100);
		*(Digitos + 2) = sal3 + 48;
		sal2 = ((bin - (sal5 * 10000) - (sal4 * 1000) - (sal3 * 100)) / 10);
		*(Digitos + 1) = sal2 + 48;
		*Digitos = (bin - ((sal5 * 10000) + (sal4 * 1000) + (sal3 * 100) + (sal2 * 10))) + 48;
	}
	else
	{
		bin = -1 * bin;
		sal5 = (bin / 10000);
		*(Digitos + 4) = '-'; // sal5+48;
		sal4 = ((bin - (sal5 * 10000)) / 1000);
		*(Digitos + 3) = sal4 + 48;
		sal3 = ((bin - (sal5 * 10000) - (sal4 * 1000)) / 100);
		*(Digitos + 2) = sal3 + 48;
		sal2 = ((bin - (sal5 * 10000) - (sal4 * 1000) - (sal3 * 100)) / 10);
		*(Digitos + 1) = sal2 + 48;
		*Digitos = (bin - ((sal5 * 10000) + (sal4 * 1000) + (sal3 * 100) + (sal2 * 10))) + 48;
	}
	asm("wdr");
}

void uint16ToAscii (uint16_t bin, char* Digitos)
{
	unsigned char sal2, sal3, sal4, sal5;
	sal5 = (bin / 10000);
	*(Digitos + 4) = sal5+48;
	sal4 = ((bin - (sal5 * 10000)) / 1000);
	*(Digitos + 3) = sal4 + 48;
	sal3 = ((bin - (sal5 * 10000) - (sal4 * 1000)) / 100);
	*(Digitos + 2) = sal3 + 48;
	sal2 = ((bin - (sal5 * 10000) - (sal4 * 1000) - (sal3 * 100)) / 10);
	*(Digitos + 1) = sal2 + 48;
	*Digitos = (bin - ((sal5 * 10000) + (sal4 * 1000) + (sal3 * 100) + (sal2 * 10))) + 48;
	*(Digitos+5)= '\0';
	asm("wdr");
}

void uint16ToAsciiInv (uint16_t bin, char* Digitos)
{
	unsigned char sal2, sal3, sal4, sal5;
	sal5 = (bin / 10000);
	*Digitos = sal5+48;
	sal4 = ((bin - (sal5 * 10000)) / 1000);
	*(Digitos + 1) = sal4 + 48;
	sal3 = ((bin - (sal5 * 10000) - (sal4 * 1000)) / 100);
	*(Digitos + 2) = sal3 + 48;
	sal2 = ((bin - (sal5 * 10000) - (sal4 * 1000) - (sal3 * 100)) / 10);
	*(Digitos + 3) = sal2 + 48;
	*(Digitos + 4) = (bin - ((sal5 * 10000) + (sal4 * 1000) + (sal3 * 100) + (sal2 * 10))) + 48;
	*(Digitos+5)= '\0';
	asm("wdr");
}

void uintToAsciiGPRS (uint16_t bin, char* Digitos)
{
	unsigned char sal2;
	sal2 = (bin / 10);
	*(Digitos) = sal2+48;
	*(Digitos+1)= (bin - sal2*10)+48;
	*(Digitos+2)= '\0';
	asm("wdr");
}


// reverses a string 'str' of length 'len'
void reverse(char *str, int len)
{
	int i=0, j=len-1, temp;
	while (i<j)
	{
		temp = str[i];
		str[i] = str[j];
		str[j] = temp;
		i++; j--;
	}
}



 // Converts a given integer x to string str[].  d is the number
 // of digits required in output. If d is more than the number
 // of digits in x, then 0s are added at the beginning.
 int intToStr(int x, char str[], int d)
 {
	 int i = 0;
	 while (x)
	 {
		 str[i++] = (x%10) + '0';
		 x = x/10;
	 }
	 
	 // If number of digits required is more, then
	 // add 0s at the beginning
	 while (i < d)
	 str[i++] = '0';
	 
	 reverse(str, i);
	 str[i] = '\0';
	 return i;
 }


char *ftoa(char *buffer, double d, int precision) 
{
	long wholePart = (long) d;

	// Deposit the whole part of the number.

	itoa(wholePart,buffer,10);

	// Now work on the faction if we need one.

	if (precision > 0) {

		// We do, so locate the end of the string and insert
		// a decimal point.

		char *endOfString = buffer;
		while (*endOfString != '\0') endOfString++;
		*endOfString++ = '.';

		// Now work on the fraction, be sure to turn any negative
		// values positive.

		if (d < 0) {
			d *= -1;
			wholePart *= -1;
		}
		
		double fraction = d - wholePart;
		while (precision > 0) {

			// Multipleby ten and pull out the digit.

			fraction *= 10;
			wholePart = (long) fraction;
			*endOfString++ = '0' + wholePart;

			// Update the fraction and move on to the
			// next digit.

			fraction -= wholePart;
			precision--;
		}

		// Terminate the string.

		*endOfString = '\0';
	}

	return buffer;
}