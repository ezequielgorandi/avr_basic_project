
/**
@defgroup gorandi_main Main
@date 13/05/2017
 *  Author: Ezequiel Gorandi
 *  e-mail: ezequielgorandi@gmail.com
@version 1.00
*/
/**@{*/
	
#include "main.h"
#include <avr/fuse.h>
#define UART_BAUD_RATE      9600

FUSES =
{
	.low = 0xFF,
	.high =0xD1,
	.extended = 0xFD,
};


/**
 @brief Inicialización de los puertos entrada/salida.
 @param void
 @return void
1*/
void port_init(void)
{
	PORTA = 0x00;
	DDRA  = 0x00;
	PORTB = 0x00;
	DDRB  = 0x00;
	PORTC = 0x00; //m103 output only
	DDRC  = 0x00;
	PORTD = 0x00;
	DDRD  = 0x00;
	PORTE = 0x00;
	DDRE  = 0x00;
	PORTF = 0x00;
	DDRF  = 0x00;
	PORTG = 0x00;
	DDRG  = 0x00;
	PORTH = 0x00;
	DDRH  = 0x00;
	PORTJ = 0x00;
	DDRJ  = 0x00;
	PORTK = 0x00;
	DDRK  = 0x00;
	PORTL = 0x00;
	DDRL  = 0x00;
}


/**
 @brief Inicialización de todos los componentes del programa.
 @param void
 @return void
*/
void init_devices ( void )
{
	//stop errant interrupts until set up
	cli(); //disable all interrupts
	port_init();
	uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) ); 
//	uart1_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) );
//	uart2_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) );
//	uart3_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) );
	init_Timer0();
//	init_Timer1();
//	initAdc();
//	i2c_init();
//	spi_init();
	MCUCR = 0x00;
	//set_sleep_mode(SLEEP_MODE_IDLE);
	sei(); //re-enable interrupts
}

/**
 @brief Main del programa
 @param void
 @return void
 @note 

*/
int main(void)
{	
	
//************************** DEFINICIÓN DE VARIABLES ***************************
	init_devices();
//	lcd_init();
//	 _delay_ms(100);
//	 lcd_clear();
//	 lcd_putstr_P(PSTR("Hola Mundo"));
	
//***************************** START PROGRAM **********************************
	//--- Inicializacion de los timeOut  ---

	//--- Set TimeOuts ---
	setTimeOut_S_Timer1(UART_TIMEOUT,3);
	//--- End of Set TimeOuts ---

	while(1)
	{
		if (timeOut_Timer1(UART_TIMEOUT) )
		{
			setTimeOut_S_Timer1(UART_TIMEOUT,3);			
			uart_puts("Hola Mundo");
		}	
	}
}
