# README #
# Proyecto básico para ATMEGA2560
### Tecnologia Utilizada 

uC ATMEGA 2560

Compilador GCC-AVR (Atmel Studio).

### Perifericos ###

- I2C
- SPI
- UART
- Timers
- ADC
- Display 2x16

# **Datos del proyecto** #
### Fecha de inicio: 10/04/2018 ###

## Versiones ##

### Version 1.00 ###
* Fecha 10/04/2018

### Who do I talk to? ###
* Ezequiel Gorandi ezequielgorandi@gmail.com